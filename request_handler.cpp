#include "dom_serializer.h"
#include "request_error.h"
#include "request_handler.h"

namespace domserver {

void RequestHandler::createResponse(
    const RequestHandlerTy& Req,
    const std::string& Body,
    const restinio::http_status_line_t& Status = restinio::status_ok()) const {
    Req->create_response(Status)
        .append_header_date_field()
        .append_header(restinio::http_field::content_type,
                       "application/json")
        .set_body(Body)
        .done();
}


void RequestHandler::processPostOrder(const RequestHandlerTy& Req) const {
    OrderData OD;

    try {
        OD = json_dto::from_json<OrderData>(Req->body());
    }
    catch (const json_dto::ex_t& E) {
        createResponse(Req, json_dto::to_json(RequestError(E.what())),
                       restinio::status_bad_request());

        return;
    }

    // Underlying conditions could be avoided by using return codes from
    // storage objects. Left in this form for simplicity.
    if (!OD.isValid()) {
        createResponse(Req, json_dto::to_json(RequestError(
                           "Order is not valid")),
                       restinio::status_bad_request());
        return;
    }

    auto AddedOrder = DomDataRef.addOrder(std::move(OD));
    if (!AddedOrder) {
        createResponse(Req, json_dto::to_json(RequestError(
                           "An error occured during order adding")),
                       restinio::status_bad_request());
        return;
    }

    createResponse(Req, json_dto::to_json(OrderWithIdWrapper(
        AddedOrder->get())));
}

void RequestHandler::processDeleteOrder(const RequestHandlerTy& Req,
                                        uint64_t Id) const {

    if (DomDataRef.deleteOrder(Id)) {
        createResponse(Req, {});
    } else {
        createResponse(Req, json_dto::to_json(RequestError(fmt::format(
                           "Order with id={} was not found", Id))),
                       restinio::status_bad_request());
    }
}

void RequestHandler::processGetOrder(const RequestHandlerTy& Req,
                                     uint64_t Id) const {
    if (auto Order = DomDataRef.getOrder(Id)) {
        createResponse(Req, json_dto::to_json(OrderWithIdWrapper(
            Order->get())));
    } else {
        createResponse(Req, json_dto::to_json(RequestError(fmt::format(
                           "Order with id={} was not found", Id))),
                       restinio::status_bad_request());
    }
}

void RequestHandler::processGetMarketData(const RequestHandlerTy& Req) const {
    auto&& DepthData = DomDataRef.getDepth();

    createResponse(Req, json_dto::to_json(DepthDataWrapper(
        std::move(DepthData))));
}

std::unique_ptr<RouterTy> createServerHandler(const RequestHandler& Handler) {
    auto Router = std::make_unique<domserver::RouterTy>();

    Router->http_post(
        PostOrderRoute,
        [Handler] (const auto& Req, const auto&) {
            Handler.processPostOrder(Req);

            return restinio::request_accepted();
        });

    Router->http_delete(
        DeleteOrderRoute,
        [Handler] (const auto& Req, const auto& Params) {
            Handler.processDeleteOrder(Req,
                                       restinio::cast_to<uint64_t>(Params["id"]));

            return restinio::request_accepted();
        });

    Router->http_get(
        GetOrderRoute,
        [Handler] (const auto& Req, const auto& Params) {
            Handler.processGetOrder(Req,
                                    restinio::cast_to<uint64_t>(Params["id"]));

            return restinio::request_accepted();
        });

    Router->http_get(
        GetMarketDataRoute,
        [Handler] (const auto& Req, const auto&) {
            Handler.processGetMarketData(Req);

            return restinio::request_accepted();
        });

    return Router;
}

void runServer() {
    DomData DD;
    RequestHandler Handler(DD);

    using MyTraitsTy =
        restinio::traits_t<
            restinio::asio_timer_manager_t,
            restinio::single_threaded_ostream_logger_t,
            domserver::RouterTy>;

    restinio::run(
        restinio::on_thread_pool<MyTraitsTy>(11)
        .port(8080)
        .address("localhost")
        .request_handler(createServerHandler(Handler)));
}

} // end namespace domserver
