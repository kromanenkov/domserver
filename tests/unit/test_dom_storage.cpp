#include "gtest/gtest.h"
#include "dom_storage.h"

#include <algorithm>
#include <random>
#include <vector>

using OrderType = domserver::OrderData::OrderType;

bool isEmpty(const domserver::DomData& DD) {
    const auto& [Asks, Bids] = DD.getDepth();

    return Asks.empty() && Bids.empty();
}

bool checkOrder(const domserver::OrderWithId &Lhs,
                const domserver::OrderWithId &Rhs) {
    EXPECT_EQ(Lhs.Id, Rhs.Id);
    EXPECT_EQ(Lhs.Order, Rhs.Order);

    return !testing::Test::HasFailure();
}

TEST(OrderData, Validness) {
    domserver::OrderData OD1{.Price = -1.0, .Quantity = 0.5,
                             .Type = OrderType::Bid};
    EXPECT_TRUE(OD1.isValid());

    domserver::OrderData OD2{.Price = 0.0000000001, .Quantity = 1.0,
                             .Type = OrderType::Ask};
    EXPECT_TRUE(OD2.isValid());

    domserver::OrderData OD3{.Price = 1e-30, .Quantity = 1.0,
                             .Type = OrderType::Bid};
    EXPECT_FALSE(OD3.isValid());

    domserver::OrderData OD4{.Price = 1.0, .Quantity = -1.0,
                             .Type = OrderType::Bid};
    EXPECT_FALSE(OD4.isValid());

    domserver::OrderData OD5{.Price = 1.0, .Quantity = 1.0,
                             .Type = OrderType::Unknown};
    EXPECT_FALSE(OD4.isValid());
}

TEST(Default, Empty) {
    domserver::DomData DD;
    EXPECT_TRUE(isEmpty(DD));
}

// Tests addOrder
TEST(Add, TrivialAddAsk) {
    domserver::DomData DD;

    auto AddedOrder = DD.addOrder({.Price = 42, .Quantity = 3,
                                   .Type = OrderType::Ask});
    EXPECT_TRUE(AddedOrder);

    const auto& [Asks, Bids] = DD.getDepth();
    EXPECT_EQ(Bids.size(), 0);
    EXPECT_EQ(Asks.size(), 1);

    EXPECT_TRUE(checkOrder(Asks[0], {.Id = AddedOrder->get().Id,
                                     .Price = 42, .Quantity = 3,
                                     .Type = OrderType::Ask}));
    EXPECT_TRUE(checkOrder(Asks[0], *AddedOrder));
}

TEST(Add, TrivialAddBid) {
    domserver::DomData DD;

    auto AddedOrder = DD.addOrder({.Price = 1.5, .Quantity = 1,
                                   .Type = OrderType::Bid});

    const auto& [Asks, Bids] = DD.getDepth();
    EXPECT_EQ(Asks.size(), 0);
    EXPECT_EQ(Bids.size(), 1);

    EXPECT_TRUE(checkOrder(Bids[0], {.Id = AddedOrder->get().Id,
                                     .Price = 1.5, .Quantity = 1,
                                     .Type = OrderType::Bid}));
    EXPECT_TRUE(checkOrder(Bids[0], *AddedOrder));
}

TEST(Add, MixedAdd) {
    domserver::DomData DD;

    DD.addOrder({.Price = 0.100000001, .Quantity = 1, OrderType::Ask});
    DD.addOrder({.Price = 2.0,         .Quantity = 2, OrderType::Bid});
    DD.addOrder({.Price = 0.1,         .Quantity = 3, OrderType::Ask});
    DD.addOrder({.Price = 2.0,         .Quantity = 4, OrderType::Bid});
    DD.addOrder({.Price = 1.5,         .Quantity = 5, OrderType::Bid});
    DD.addOrder({.Price = 1.0,         .Quantity = 6, OrderType::Ask});

    const auto& [Asks, Bids] = DD.getDepth();

    EXPECT_EQ(Asks.size(), 3);
    EXPECT_EQ(Bids.size(), 3);

    EXPECT_TRUE(checkOrder(Asks[0], {.Id = 3, .Price = 0.1, .Quantity = 3,
                                     .Type = OrderType::Ask}));
    EXPECT_TRUE(checkOrder(Asks[1], {.Id = 1, .Price = 0.100000001, .Quantity = 1,
                                     .Type = OrderType::Ask}));
    EXPECT_TRUE(checkOrder(Asks[2], {.Id = 6, .Price = 1.0, .Quantity = 6,
                                     .Type = OrderType::Ask}));

    EXPECT_TRUE(checkOrder(Bids[0], {.Id = 5, .Price = 1.5, .Quantity = 5,
                                     .Type = OrderType::Bid}));
    EXPECT_TRUE(checkOrder(Bids[1], {.Id = 2, .Price = 2.0, .Quantity = 2,
                                     .Type = OrderType::Bid}));
    EXPECT_TRUE(checkOrder(Bids[2], {.Id = 4, .Price = 2.0, .Quantity = 4,
                                     .Type = OrderType::Bid}));
}

// Tests getOrder
TEST(Get, Empty) {
    domserver::DomData DD;

    auto Order = DD.getOrder(1);
    EXPECT_FALSE(Order);
}

TEST(Get, GetFromAdd) {
    domserver::DomData DD;

    auto AddedOrder = DD.addOrder({.Price = 1.0, .Quantity = 1, OrderType::Bid});
    ASSERT_TRUE(AddedOrder);

    auto GetOrder = DD.getOrder(AddedOrder->get().Id);
    ASSERT_TRUE(GetOrder);

    EXPECT_TRUE(checkOrder(*AddedOrder, *GetOrder));
    EXPECT_TRUE(checkOrder(*AddedOrder, {.Id = AddedOrder->get().Id,
                                         .Price = 1.0, .Quantity = 1,
                                         .Type = OrderType::Bid}));

    auto GetNonExistingOrder = DD.getOrder(AddedOrder->get().Id + 1);
    EXPECT_FALSE(GetNonExistingOrder);
}

TEST(Get, DoubleGet) {
    domserver::DomData DD;

    auto AddedOrder = DD.addOrder({.Price = 1.0, .Quantity = 1, OrderType::Bid});

    auto GetOrder = DD.getOrder(AddedOrder->get().Id);
    auto GetAnotherOrder = DD.getOrder(AddedOrder->get().Id);

    EXPECT_TRUE(checkOrder(GetOrder->get(), GetAnotherOrder->get()));
}

// Tests deleteOrder
TEST(Delete, Empty) {
    domserver::DomData DD;

    DD.deleteOrder(1);
    EXPECT_TRUE(isEmpty(DD));
}

TEST(Delete, DeleteFromAdd) {
    domserver::DomData DD;

    auto AddedOrder = DD.addOrder({.Price = 1.0, .Quantity = 1, OrderType::Bid});
    EXPECT_FALSE(isEmpty(DD));

    DD.deleteOrder(AddedOrder->get().Id);
    EXPECT_TRUE(isEmpty(DD));
}

TEST(Delete, DeleteOneOfAdd) {
    domserver::DomData DD;

    auto AddedOrder = DD.addOrder({.Price = 1.0, .Quantity = 1, OrderType::Bid});
    auto AnotherAddedOrder = DD.addOrder({.Price = 2.0, .Quantity = 1, OrderType::Ask});

    DD.deleteOrder(AddedOrder->get().Id);
    const auto& [Asks, Bids] = DD.getDepth();
    EXPECT_EQ(Bids.size(), 0);
    EXPECT_EQ(Asks.size(), 1);

    EXPECT_TRUE(checkOrder(Asks[0], {.Id = AnotherAddedOrder->get().Id,
                                     .Price = 2.0, .Quantity = 1,
                                     .Type = OrderType::Ask}));
    checkOrder(Asks[0], *AnotherAddedOrder);

    // Delete non-existing element.
    DD.deleteOrder(AddedOrder->get().Id);
    const auto& [Asks2, Bids2] = DD.getDepth();
    EXPECT_EQ(Bids2.size(), 0);
    EXPECT_EQ(Asks2.size(), 1);

    EXPECT_TRUE(checkOrder(Asks2[0], {.Id = AnotherAddedOrder->get().Id,
                                      .Price = 2.0, .Quantity = 1,
                                      .Type = OrderType::Ask}));

    EXPECT_FALSE(isEmpty(DD));
}


TEST(Delete, DeleteAll) {
    domserver::DomData DD;

    auto AddedOrder = DD.addOrder({.Price = 1.0, .Quantity = 1, OrderType::Bid});
    auto AnotherAddedOrder = DD.addOrder({.Price = 2.0, .Quantity = 1, OrderType::Ask});

    DD.deleteOrder(AddedOrder->get().Id);
    DD.deleteOrder(AnotherAddedOrder->get().Id);

    EXPECT_TRUE(isEmpty(DD));
}

// Tests getDepth order
TEST(GetDepth, Order) {
    domserver::DomData DD;
    std::random_device RD;
    std::mt19937 Gen(RD());
    std::uniform_real_distribution<> Distrib(0.1, 1000.0);

    constexpr int CASES = 100000;

    std::vector<double> AsksPrices, BidsPrices;
    AsksPrices.reserve(CASES);
    BidsPrices.reserve(CASES);
    for (int i = 0; i < CASES; ++i) {
        double AskPrice = Distrib(Gen);
        double BidPrice = Distrib(Gen);

        AsksPrices.push_back(AskPrice);
        BidsPrices.push_back(BidPrice);

        DD.addOrder({.Price = AskPrice, .Quantity = 1, OrderType::Ask});
        DD.addOrder({.Price = BidPrice, .Quantity = 1, OrderType::Bid});
    }

    std::sort(AsksPrices.begin(), AsksPrices.end());
    std::sort(BidsPrices.begin(), BidsPrices.end());

    const auto& [Asks, Bids] = DD.getDepth();
    EXPECT_EQ(Bids.size(), CASES);
    EXPECT_EQ(Asks.size(), CASES);

    std::vector<double> DOMAsksPrices, DOMBidsPrices;
    DOMAsksPrices.reserve(CASES);
    DOMBidsPrices.reserve(CASES);
    std::transform(Asks.begin(), Asks.end(), std::back_inserter(DOMAsksPrices),
                   [](const auto& O) { return O.Order.Price; });
    std::transform(Bids.begin(), Bids.end(), std::back_inserter(DOMBidsPrices),
                   [](const auto& O) { return O.Order.Price; });

    EXPECT_EQ(AsksPrices, DOMAsksPrices);
    EXPECT_EQ(BidsPrices, DOMBidsPrices);
}

