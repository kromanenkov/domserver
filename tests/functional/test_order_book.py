#!/usr/bin/env python3

from utils import run_server, Request, get_depth_data, assert_depth_is_empty, BASIC_URL, OK_REQUEST_STATUS, \
                  BAD_REQUEST_STATUS, NOT_IMPLEMENTED_STATUS

import pytest

import logging
import random


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('test_domserver')


class TestAdd:
    @pytest.mark.parametrize('order', [{'price': 1.0, 'quantity': 1.0, 'order_type': 'Bid'},
                                       {'price': -42.0, 'quantity': 0.5, 'order_type': 'ASK'},
                                       {'price': 1e-10, 'quantity': 1e100, 'order_type': 'BiD'},
                                       {'price': 0.5, 'quantity': 10.0, 'order_type': 'aSK'},
                                       {'price': 1.0, 'quantity': 1.0, 'order_type': 'bid', 'extra_param': 'extra_value'}])
    def test_basic(self, order):
        response = Request(method='POST', json=order).perform()
        logger.debug(response.json())

        assert response.status_code == OK_REQUEST_STATUS
        assert set(response.json().keys()) == set(['order'])
        assert response.url == f'{BASIC_URL}/orders'

        added_order = response.json()['order']
        added_id = added_order.pop('id')
        assert added_id == 1
        # order_type case is nonessential.
        order['order_type'] = order['order_type'].lower()

        try:
            del order['extra_param']
        except KeyError:
            pass
        assert added_order == order

    @pytest.mark.parametrize('order', [{}, {'price': 1}, {'quanitity': 1}, {'order_type': 'ask'},
                                       {'price': 1, 'quantity': 1}, {'price': 1, 'order_type': 'bid'},
                                       {'quantity': 1, 'order_type': 'ask'},
                                       {'price': '1.0', 'quantity': 1, 'order_type': 'bid'},
                                       {'price': [1], 'quantity': 1, 'order_type': 'bid'},
                                       {'price': 1, 'quantity': '1.0', 'order_type': 'ask'},
                                       {'price': 1, 'quantity': [1], 'order_type': 'ask'},
                                       {'price': 1, 'quantity': 1, 'order_type': ['bid']},
                                       {'price': 1, 'quantity': 1, 'order_type': 'bad'},
                                       {'price': 1, 'quantity': {'order_type': 'ask'}}])
    def test_bad_body(self, order):
        response = Request(method='POST', json=order).perform()
        logger.debug(response.json())

        assert response.status_code == BAD_REQUEST_STATUS
        assert set(response.json().keys()) == set(['error'])
        assert response.json()['error'].startswith('error reading field')

    @pytest.mark.parametrize('order', [{'price': 1e-30, 'quantity': 1, 'order_type': 'ask'},
                                       {'price': 1, 'quantity': -1, 'order_type': 'bid'},
                                       {'price': 1, 'quantity': 1, 'order_type': 'unknown'}])
    def test_invalid_order(self, order):
        response = Request(method='POST', json=order).perform()
        logger.debug(response.json())

        assert response.status_code == BAD_REQUEST_STATUS
        assert response.json()['error'] == 'Order is not valid'

    def test_multiple_add(self):
        response = Request(method='POST', json={'price': 1, 'quantity': 1, 'order_type': 'ask'}).perform()
        assert response.status_code == OK_REQUEST_STATUS

        response = Request(method='POST', json={'price': 1, 'quantity': 1, 'order_type': 'bid'}).perform()
        assert response.status_code == OK_REQUEST_STATUS

        depth_data = get_depth_data()
        assert len(depth_data['asks']) == 1
        assert len(depth_data['bids']) == 1


class TestDelete:
    def test_basic(self):
        added_order = Request(method='POST', json={'price': 1, 'quantity': 1, 'order_type': 'ask'}).perform().json()
        response = Request(method='DELETE', path=f'/orders/{added_order["order"]["id"]}').perform()

        assert response.status_code == OK_REQUEST_STATUS
        assert response.url == f'{BASIC_URL}/orders/{added_order["order"]["id"]}'
        assert response.content == b''

    @pytest.mark.parametrize('order_id', [0, 1, 10, 100])
    def test_bad_id(self, order_id):
        response = Request(method='DELETE', path=f'/orders/{order_id}').perform()

        assert response.status_code == BAD_REQUEST_STATUS
        assert set(response.json().keys()) == set(['error'])
        assert response.json()['error'] == f'Order with id={order_id} was not found'

    def test_double_delete(self):
        added_order = Request(method='POST', json={'price': 1, 'quantity': 1, 'order_type': 'ask'}).perform().json()
        order_id = added_order['order']['id']
        Request(method='DELETE', path=f'/orders/{order_id}').perform()
        response = Request(method='DELETE', path=f'/orders/{order_id}').perform()

        assert response.status_code == BAD_REQUEST_STATUS
        assert response.json()['error'] == f'Order with id={order_id} was not found'

    def test_multiple_delete(self):
        added_ids = []

        case_generator = range(1, 50)
        for price in case_generator:
            added_order = Request(method='POST', json={'price': price, 'quantity': 1,
                                                       'order_type': random.choice(['bid', 'ask'])}).perform().json()
            added_ids.append(added_order['order']['id'])
        depth_data = get_depth_data()
        assert len(depth_data['bids']) + len(depth_data['asks']) == len(case_generator)

        for order_id in case_generator:
            delete_response = Request(method='DELETE', path=f'/orders/{order_id}').perform()
            assert delete_response.status_code == OK_REQUEST_STATUS

        assert_depth_is_empty()


class TestGet:
    @pytest.mark.parametrize('order', [{'price': 1.0, 'quantity': 1.0, 'order_type': 'bid'},
                                       {'price': -42.0, 'quantity': 0.5, 'order_type': 'ask'},
                                       {'price': 1e-10, 'quantity': 1e100, 'order_type': 'bid'},
                                       {'price': 0.5, 'quantity': 10.0, 'order_type': 'ask'}])
    def test_basic(self, order):
        add_response = Request(method='POST', json=order).perform().json()
        order_id = add_response['order']['id']
        get_response = Request(path=f'/orders/{order_id}').perform()

        assert get_response.status_code == OK_REQUEST_STATUS
        assert get_response.url == f'{BASIC_URL}/orders/{order_id}'

        added_order = add_response['order']
        del added_order['id']
        assert added_order == order

    @pytest.mark.parametrize('order_id', [0, 1, 10, 100])
    def test_bad_id(self, order_id):
        response = Request(path=f'/orders/{order_id}').perform()

        assert response.status_code == BAD_REQUEST_STATUS
        assert set(response.json().keys()) == set(['error'])
        assert response.json()['error'] == f'Order with id={order_id} was not found'

    def test_double_get(self):
        order = {'price': 42.0, 'quantity': 1, 'order_type': 'ask'}
        add_response = Request(method='POST', json=order).perform().json()
        order_id = add_response['order']['id']

        get_response1 = Request(path=f'/orders/{order_id}').perform()
        assert get_response1.status_code == OK_REQUEST_STATUS
        get_response2 = Request(path=f'/orders/{order_id}').perform()
        assert get_response2.status_code == OK_REQUEST_STATUS
        assert get_response1.json() == get_response2.json()

    def test_get_after_delete(self):
        order = {'price': 42.0, 'quantity': 1, 'order_type': 'bid'}
        add_response = Request(method='POST', json=order).perform().json()
        order_id = add_response['order']['id']

        Request(method='DELETE', path=f'/orders/{order_id}').perform()
        get_response = Request(path=f'/orders/{order_id}').perform()

        assert get_response.status_code == BAD_REQUEST_STATUS
        assert get_response.json()['error'] == f'Order with id={order_id} was not found'

        assert_depth_is_empty()


class TestMarketData():
    def test_empty(self):
        assert_depth_is_empty()

    def test_basic(self):
        Request(method='POST', json={'price': 1.0, 'quantity': 42, 'order_type': 'ask'}).perform()
        Request(method='POST', json={'price': 0.1000001, 'quantity': 2, 'order_type': 'bid'}).perform()
        Request(method='POST', json={'price': 0.1, 'quantity': 10, 'order_type': 'bid'}).perform()
        Request(method='POST', json={'price': 42, 'quantity': 10, 'order_type': 'ask'}).perform()

        depth_data = get_depth_data()
        assert depth_data['asks'] == [{'price': 1.0, 'quantity': 42}, {'price': 42, 'quantity': 10}]
        assert depth_data['bids'] == [{'price': 0.1, 'quantity': 10}, {'price': 0.1000001, 'quantity': 2}]

    def test_price_sort(self):
        CASES_NUM = 1000
        asks_prices = []
        bids_prices = []

        for _ in range(CASES_NUM):
            ask_price, bid_price = round(random.uniform(0.1, 1000.0), 10), round(random.uniform(0.1, 1000.0), 10)
            Request(method='POST', json={'price': ask_price, 'quantity': 1, 'order_type': 'ask'}).perform()
            Request(method='POST', json={'price': bid_price, 'quantity': 1, 'order_type': 'bid'}).perform()

            asks_prices.append(ask_price)
            bids_prices.append(bid_price)

        asks_prices.sort()
        bids_prices.sort()

        depth_data = get_depth_data()
        assert len(depth_data['asks']) + len(depth_data['bids']) == CASES_NUM * 2

        depth_asks_prices = [ask['price'] for ask in depth_data['asks']]
        depth_bids_prices = [bid['price'] for bid in depth_data['bids']]

        assert asks_prices == depth_asks_prices
        assert bids_prices == depth_bids_prices


class TestBadRoute():
    @pytest.mark.parametrize('method, path', [('GET', '/order/-1'), ('POST', '/some/route'),
                                              ('PATCH', '/orders'), ('GET', '/marketdata'),
                                              ('DELETE', '/order/-1')])
    def test_bad_url(self, method, path):
        response = Request(method=method, path=path).perform()

        assert response.status_code == NOT_IMPLEMENTED_STATUS
        assert response.content == b''
