#!/usr/bin/env python3

import pytest

import logging
import os
import pprint
import requests
import subprocess
import time

OK_REQUEST_STATUS = 200
BAD_REQUEST_STATUS = 400
NOT_IMPLEMENTED_STATUS = 501

BINARY_RELATIVE_PATH = '../../build-dir/domserver'
BINARY_PATH = os.path.normpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), BINARY_RELATIVE_PATH))

HOST = 'localhost'
PORT = 8080
BASIC_URL = f'http://{HOST}:{PORT}/book'

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('utils')
pp = pprint.PrettyPrinter(indent=2)


@pytest.fixture(scope='function', autouse=True)
def run_server(request):
    proc = subprocess.Popen([BINARY_PATH])
    time.sleep(0.3)

    def teardown():
        proc.terminate()
        time.sleep(0.1)

    request.addfinalizer(teardown)


class Request():
    def __init__(self, method='GET', path=None, json=None):
        self.method = method
        self.path = f'{path}' if path is not None else '/orders'
        self.json = json if json is not None else {}
        self.session = requests.Session()

    def perform(self):
        request = requests.Request(self.method,
                                   url=f'{BASIC_URL}{self.path}',
                                   json=self.json).prepare()
        logger.debug('Performing http request:\n' + pp.pformat(request.__dict__))
        response = self.session.send(request, timeout=200)
        logger.debug('Got http response:\n' + pp.pformat(response.__dict__))

        return response


def get_depth_data():
    depth_response = Request(path='/market_data').perform()
    assert depth_response.status_code == OK_REQUEST_STATUS
    assert set(depth_response.json().keys()) == set(['asks', 'bids'])

    return depth_response.json()


def assert_depth_is_empty():
    depth_data = get_depth_data()

    assert depth_data['bids'] == [], 'bids array is not empty'
    assert depth_data['asks'] == [], 'asks array is not empty'
