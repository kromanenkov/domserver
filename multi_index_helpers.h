#ifndef MULTI_INDEX_HELPERS_H_
#define MULTI_INDEX_HELPERS_H_

namespace domserver {

// Helper structs for multi-index container tagging.
struct ById {};
struct ByTypeAndPrice {};

// Helper structs to deal with nested structs indexing.
template<class T, class U>
struct OrderTypeExtractor {
    using result_type = typename T::OrderType;

    result_type operator()(const U& OrderWithId) const {
        return OrderWithId.Order.Type;
    }
};

template<class U>
struct OrderPriceExtractor {
    using result_type = double;

    result_type operator()(const U& OrderWithId) const {
        return OrderWithId.Order.Price;
    }
};


} // end namespace domserver

#endif // MULTI_INDEX_HELPERS_H_

