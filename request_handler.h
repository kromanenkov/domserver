#ifndef REQUEST_HANDLER_H_
#define REQUEST_HANDLER_H_

#include "dom_storage.h"
#include "routes.h"

#include <restinio/all.hpp>

#include <memory>

namespace domserver {

using RouterTy = restinio::router::express_router_t<>;

// Class which incapsulates logic of translating HTTP requests into storage
// object methods calls.
class RequestHandler {
    using RequestHandlerTy = restinio::request_handle_t;

public:
    RequestHandler(DomData& DD) : DomDataRef(DD) {}

    void processPostOrder(const RequestHandlerTy& Req) const;
    void processDeleteOrder(const RequestHandlerTy& Req, uint64_t Id) const;
    void processGetOrder(const RequestHandlerTy& Req, uint64_t Id) const;
    void processGetMarketData(const RequestHandlerTy& Req) const;

    // Helper function for creating response using restinio.
    void createResponse(const RequestHandlerTy& Req, const std::string& Body,
                        const restinio::http_status_line_t& Status) const;
private:
    // Alias for storage object.
    DomData& DomDataRef;
};

// Set server routes.
std::unique_ptr<RouterTy> createServerHandler(const RequestHandler& Handler);

// Main entrypoint.
void runServer();

} // end namespace domserver

#endif // REQUEST_HANDLER_H_
