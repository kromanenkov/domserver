#include "dom_storage.h"

#include <iostream>

namespace domserver {

// Implementation is defined by business logic. For example, check that
// Quantity value is positive and Price is not equal to zero and
// OrderType is defined.
bool OrderData::isValid() const {
    return Quantity > 0 && std::fabs(Price) > EPS &&
           (Type == OrderType::Ask || Type == OrderType::Bid);
}

std::optional<std::reference_wrapper<const OrderWithId>> DomData::addOrder(OrderData&& O) {
    const std::lock_guard<std::mutex> Lock(Mutex);

    uint64_t Id = this->getIndex();
    auto [Iter, Inserted] = DOMContainer.emplace(Id, std::move(O));

    if (Inserted) {
        this->increaseIndex();
        return *Iter;
    }

    return {};
}

bool DomData::deleteOrder(uint64_t Id) {
    const std::lock_guard<std::mutex> Lock(Mutex);

    auto& IndexById = DOMContainer.get<ById>();
    if (auto Iter = IndexById.find(Id); Iter != IndexById.end()) {
        IndexById.erase(Iter);
        return true;
    }

    return false;
}

std::optional<std::reference_wrapper<const OrderWithId>> DomData::getOrder(uint64_t Id) const {
    const std::lock_guard<std::mutex> Lock(Mutex);

    const auto& IndexById = DOMContainer.get<ById>();
    if (auto Iter = IndexById.find(Id); Iter != IndexById.end()) {
        return *Iter;
    }

    return {};
}

DepthData DomData::getDepth() const {
    DepthData DD;

    const std::lock_guard<std::mutex> Lock(Mutex);
    const auto& IndexByTypeAndPrice =
        DOMContainer.get<domserver::ByTypeAndPrice>();

    auto AsksRange = IndexByTypeAndPrice.equal_range(boost::make_tuple(
        domserver::OrderData::OrderType::Ask));
    while (AsksRange.first != AsksRange.second) {
        DD.Asks.push_back(*(AsksRange.first));
        ++AsksRange.first;
    }

    auto BidsRange = IndexByTypeAndPrice.equal_range(boost::make_tuple(
        domserver::OrderData::OrderType::Bid));
    while (BidsRange.first != BidsRange.second) {
        DD.Bids.push_back(*(BidsRange.first));
        ++BidsRange.first;
    }

    return DD;
}

} // end namespace domserver
