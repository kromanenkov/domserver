#ifndef DOM_STORAGE_H_
#define DOM_STORAGE_H_

#include "multi_index_helpers.h"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/multi_index/composite_key.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/ordered_index.hpp>

#include <cmath>
#include <mutex>
#include <optional>
#include <vector>

namespace domserver {

using namespace boost::multi_index;

constexpr double EPS = 1e-20;

// Corresponds to the order from the user request.
struct OrderData {
    enum class OrderType {
        Ask = 0,
        Bid = 1,

        Unknown
    };

    double Price = 0.0;
    double Quantity = 0.0;
    OrderType Type = OrderType::Unknown;

    OrderData() = default;

    OrderData(double Price, double Quantity, OrderType Type) :
        Price(Price), Quantity(Quantity), Type(Type) {}

    // Implementation is defined by business logic.
    bool isValid() const;

    bool operator==(const OrderData &Other) const {
        return Type == Other.Type &&
               std::fabs(Price - Other.Price) < EPS &&
               std::fabs(Quantity - Other.Quantity) < EPS;
    }
};

// Corresponds to the saved order.
struct OrderWithId {
    uint64_t Id = 0;
    OrderData Order;

    OrderWithId() = default;
    OrderWithId(uint64_t Id, OrderData&& Order) :
        Id(Id), Order(std::move(Order)) {}
    OrderWithId(uint64_t Id, const OrderData& Order) :
        Id(Id), Order(Order) {}
    OrderWithId(uint64_t Id, double Price, double Quantity,
                OrderData::OrderType Type) :
        Id(Id), Order(Price, Quantity, Type) {}

    bool operator==(const OrderWithId& Other) const {
        return Id == Other.Id &&
               Order == Other.Order;
    }
};

// Holds data for the dom snapshot request.
struct DepthData {
    std::vector<OrderWithId> Asks;
    std::vector<OrderWithId> Bids;
};

class DomData {
public:
    DomData() { Index = 1; }

    // Returns reference to inserted element in case of successful add.
    std::optional<std::reference_wrapper<const OrderWithId>>
        addOrder(OrderData&& O);
    // Returns true if element was successfully deleted.
    bool deleteOrder(uint64_t Id);
    // Returns reference to contained element in case of successful lookup.
    std::optional<std::reference_wrapper<const OrderWithId>>
        getOrder(uint64_t Id) const;
    // Returns dom snapshot.
    DepthData getDepth() const;

    uint64_t getIndex() const { return Index; }
    // For supporting autoincrement-like logic.
    void increaseIndex() { ++Index; }

private:
    using ContainerTy = boost::multi_index_container<
        OrderWithId,
        indexed_by<
           hashed_unique<
               tag<ById>,
               BOOST_MULTI_INDEX_MEMBER(OrderWithId, uint64_t, Id)>,
           ordered_non_unique<
               tag<ByTypeAndPrice>,
                   composite_key<
                       OrderWithId,
                       OrderTypeExtractor<OrderData, OrderWithId>,
                       OrderPriceExtractor<OrderWithId>>>>>;

    ContainerTy DOMContainer;
    mutable std::mutex Mutex;
    uint64_t Index;
};

} // end namespace domserver

#endif // DOM_STORAGE_H_
