#include "dom_storage.h"
#include "request_handler.h"

#include <restinio/all.hpp>

#include <iostream>

int main(int argc, char* argv[]) {
    try {
        domserver::runServer();
    }
    catch (const std::exception& e) {
        std::cout << "Exception was caught: " << e.what() << '\n';
        return 1;
    }
    catch (...) {
        std::cout << "Unknown exception was caught\n";
        return 2;
    }

    return 0;
}
