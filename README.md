### Install instructions

1. Install vcpkg
2. Install *restinio* using vcpkg: `vcpkg install restinio`
3. Install *json-dto* using vcpkg: `vcpkg install json-dto`
4. `cd <build_dir>` and run
   ```
   cmake -DCMAKE_TOOLCHAIN_FILE=<path_to_vcpkg>/scripts/buildsystems/vcpkg.cmake -DCMAKE_BUILD_TYPE=Release <path_to_sources>
   ```
5. `cmake --build <build_dir>`

### Unit tests instructions
1. `cd tests/unit`
2. `make all`

### Functional tests instructions
1. Build domserver (see "Install instructions").
2. `cd tests/functional`
3. Change `BINARY_RELATIVE_PATH` in `utils.py` according to the actual relative path to the `domserver` binary from the tests folder.
4. `pytest test_order_book.py` or `pytest -s test_order_book.py` to see debug output.
