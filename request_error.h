#ifndef REQUEST_ERROR_H_
#define REQUEST_ERROR_H_

#include <json_dto/pub.hpp>

#include <string>

namespace domserver {

// Helper class for serializing error messages received during request
// processing.
struct RequestError {
    std::string Description;

    explicit RequestError(const std::string& Description):
        Description(Description) {}

    template<typename Json_Io>
    void json_io(Json_Io& IO) {
        IO & json_dto::mandatory("error", Description);
    }
};

} // end namespace domserver

#endif // REQUEST_ERROR_H_
