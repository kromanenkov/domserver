#ifndef DOM_SERIALIZER_H_
#define DOM_SERIALIZER_H_

#include "dom_storage.h"

#include <json_dto/pub.hpp>

#include <algorithm>
#include <vector>

namespace domserver {

// Helper function for serialize/deserialize OrderType enum.
void read_json_value(OrderData::OrderType& Value,
                     const rapidjson::Value& From) {
    using json_dto::read_json_value;
    using OrderType = OrderData::OrderType;

    std::string Representation;
    read_json_value(Representation, From);

    // Convert order type to lower-case for simplicity.
    std::transform(Representation.begin(), Representation.end(),
                   Representation.begin(),
                   [](unsigned char c) {return ::tolower(c);});

    if (Representation == "ask")
        Value = OrderType::Ask;
    else if (Representation == "bid")
        Value = OrderType::Bid;
    else if (Representation == "unknown")
        Value = OrderType::Unknown;
    else
        throw std::runtime_error({"Invalid order type: " + Representation});
}

// Helper function for serialize/deserialize OrderType enum.
void write_json_value(const OrderData::OrderType& Value, rapidjson::Value& Object,
                      rapidjson::MemoryPoolAllocator<>& Allocator) {
    using json_dto::write_json_value;
    using OrderType = OrderData::OrderType;

    std::string Representation{(Value == OrderType::Ask)? "ask" :
                               ((Value == OrderType::Bid)? "bid" : "unknown")};
    write_json_value(Representation, Object, Allocator);
}

// A helper struct to serialize OrderWithId in {"order": {...}} form.
struct OrderWithIdWrapper {
    OrderWithId OWI;

    explicit OrderWithIdWrapper(const OrderWithId& OWI): OWI(OWI) {}
};

// A helper struct to serialize only order's price and quantity fields for depth
// request.
struct OrderForDepthWrapper {
    OrderWithId OWI;

    OrderForDepthWrapper(OrderWithId&& O) {
        OWI = std::move(O);
    }
};

// A helper struct for serializing vectors with OrderForDepthWrapper objects.
struct DepthDataWrapper {
    std::vector<OrderForDepthWrapper> Asks;
    std::vector<OrderForDepthWrapper> Bids;

    explicit DepthDataWrapper(DepthData&& DD) {
        Asks.reserve(DD.Asks.size());
        Bids.reserve(DD.Bids.size());

        std::move(DD.Asks.begin(), DD.Asks.end(), std::back_inserter(Asks));
        std::move(DD.Bids.begin(), DD.Bids.end(), std::back_inserter(Bids));
    }
};

} // end namespace domserver

namespace json_dto {

template<typename Json_Io>
void json_io(Json_Io& IO, domserver::OrderData& OD) {
    IO & json_dto::mandatory("price", OD.Price)
       & json_dto::mandatory("quantity", OD.Quantity)
       & json_dto::mandatory("order_type", OD.Type);
}

template<typename Json_Io>
void json_io(Json_Io& IO, domserver::OrderWithIdWrapper& OWIW) {
    IO & json_dto::mandatory("order", OWIW.OWI);
}

template<typename Json_Io>
void json_io(Json_Io& IO, domserver::OrderWithId& OWI) {
    IO & json_dto::mandatory("id", OWI.Id);
    json_dto::json_io(IO, OWI.Order);
}

template<typename Json_Io>
void json_io(Json_Io& IO, domserver::OrderForDepthWrapper& OFDW) {
    IO & json_dto::mandatory("price", OFDW.OWI.Order.Price)
       & json_dto::mandatory("quantity", OFDW.OWI.Order.Quantity);
}

template<typename Json_Io>
void json_io(Json_Io & IO, domserver::DepthDataWrapper& DDW) {
    IO & json_dto::mandatory("asks", DDW.Asks)
       & json_dto::mandatory("bids", DDW.Bids);
}

} // end namespace json_dto

#endif // DOM_SERIALIZER_H_
