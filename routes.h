#ifndef ROUTES_H_
#define ROUTES_H_

#include <string>

namespace domserver {

static constexpr const char* PostOrderRoute = "/book/orders";
static constexpr const char* DeleteOrderRoute = R"--(/book/orders/:id(\d+))--";
static constexpr const char* GetOrderRoute = R"--(/book/orders/:id(\d+))--";
static constexpr const char* GetMarketDataRoute = "/book/market_data";

} // end namespace domserver

#endif // ROUTES_H_
